#!/usr/bin/python
# -*- coding: utf-8 -*-
import logging
import sys
import time

from board import SCL, SDA
import busio

# Import the PCA9685 module.
from adafruit_pca9685 import PCA9685

from common import (
    PWM_CH,
    WAIT_TIME,
    PWM_FREQ,
    PWM_MAX,
    speedSteps,
    tempSteps,
    hyst,
    log_filename,
    convert_pc,
    get_new_speed,
)


if __name__ == "__main__":
    i = 0
    temp = 0
    fanSpeed = 0
    temp_old = 0
    fanSpeedOld = 0

    logging.basicConfig(
        format="%(asctime)s %(levelname)s: %(message)s",
        filename=log_filename,
        filemode="w",
        level=logging.getLevelName("INFO"),
    )
    logger = logging.getLogger(__name__)

    # Create the I2C bus interface.
    i2c_bus = busio.I2C(SCL, SDA)
    pca = PCA9685(i2c_bus)
    pca.frequency = PWM_FREQ

    # We must set a speed value for each temperature step
    assert len(speedSteps) == len(tempSteps)

    try:
        while 1:
            # Read CPU temperature
            cpuTempFile = open("/sys/class/thermal/thermal_zone0/temp", "r")
            temp = float(cpuTempFile.read()) / 1000
            cpuTempFile.close()

            if abs(temp - temp_old) > hyst:
                fanSpeed = get_new_speed(temp, tempSteps, speedSteps)

                if fanSpeed != fanSpeedOld:
                    pca.channels[PWM_CH].duty_cycle = convert_pc(fanSpeed, PWM_MAX)
                    logger.info("Temp: '%s'. Fan speed: %s" % (temp, fanSpeed))
                    fanSpeedOld = fanSpeed
                temp_old = temp

            # Wait until next refresh
            time.sleep(WAIT_TIME)

    # If a keyboard interrupt occurs (ctrl + c), the program exits.
    except KeyboardInterrupt:
        print("Fan ctrl interrupted by keyboard")
        sys.exit()
