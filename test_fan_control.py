import unittest
from common import PWM_MAX, tempSteps, speedSteps, convert_pc, get_new_speed


class TestConvertPC(unittest.TestCase):
    def test_when_value_zero_per_cent_then_convert_pc_returns_zero(self):
        test_per_cent_val = 0
        self.assertEqual(0, convert_pc(test_per_cent_val, PWM_MAX))

    def test_when_value_100_per_cent_then_convert_pc_returns_max(self):
        test_per_cent_val = 100
        self.assertEqual(PWM_MAX, convert_pc(test_per_cent_val, PWM_MAX))

    def test_when_value_over_100_then_convert_pc_returns_max(self):
        test_per_cent_val = 101
        self.assertEqual(PWM_MAX, convert_pc(test_per_cent_val, PWM_MAX))

    def test_when_value_negative_then_convert_pc_returns_zero(self):
        test_per_cent_val = -1
        self.assertEqual(0, convert_pc(test_per_cent_val, PWM_MAX))

    def test_when_value_50_per_cent_then_convert_pc_returns_half_of_max(self):
        test_per_cent_val = 50
        self.assertEqual(int(PWM_MAX / 2), convert_pc(test_per_cent_val, PWM_MAX))


class TestGetNewSpeed(unittest.TestCase):
    def test_when__min_temp__then__get_new_speed_returns_zero(self):
        temp = tempSteps[0]
        fan_speed = get_new_speed(temp, tempSteps, speedSteps)
        self.assertEqual(0, fan_speed)

    def test_when__max_temp__then__get_new_speed_returns_100(self):
        temp = tempSteps[1]
        fan_speed = get_new_speed(temp, tempSteps, speedSteps)
        self.assertEqual(100, fan_speed)


if __name__ == "__main__":
    unittest.main()
