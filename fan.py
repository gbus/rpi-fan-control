#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
from board import SCL, SDA
import busio

# Import the PCA9685 module.
from adafruit_pca9685 import PCA9685
from common import PWM_CH, PWM_FREQ, PWM_MAX, convert_pc


# Create the I2C bus interface.
i2c_bus = busio.I2C(SCL, SDA)

# Create a simple PCA9685 class instance.
pca = PCA9685(i2c_bus)

# Set the PWM frequency to 60hz.
pca.frequency = PWM_FREQ


if __name__ == "__main__":
    try:
        while 1:
            fanSpeed = int(input("Fan Speed 0-100%: "))
            pca.channels[PWM_CH].duty_cycle = convert_pc(fanSpeed, PWM_MAX)

    except KeyboardInterrupt:
        print("Fan ctrl interrupted by keyboard")
        sys.exit()
