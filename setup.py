#!/usr/bin/env python

from distutils.core import setup

setup(
    name="rpi-fan-control",
    version="1.0",
    description="Monitor the Raspberry PI cpu temperature and control the speed of the fan by PWM signal",
    author="Gianluca Busiello",
    author_email="gianluca.busiello@gmail.com",
)
