PWM_CH = 4
WAIT_TIME = 1
PWM_FREQ = 60
PWM_MAX = 65535
log_filename = "/var/log/rpi-fan-control.log"

# Configurable temperature and fan speed steps
tempSteps = [50, 70]  # [°C]
speedSteps = [0, 100]  # [%]

# Fan speed will change only if the difference of temperature is higher than hysteresis
hyst = 3


def convert_pc(per_cent: int, max_val: int) -> int:
    """
    Converts percent value to a pwm value 0-65535
    :param per_cent: value between 0-100
    :param max_val: max value for pwm input
    :return: pwm value proportional to given percentage
    """
    if per_cent > 100:
        per_cent = 100
    if per_cent < 0:
        per_cent = 0
    return int(per_cent * max_val / 100)


def get_new_speed(temp: float, tempSteps: list, speedSteps: list) -> int:
    """
    Calculate fan speed
    :param temp: Current CPU temperature
    :param tempSteps: Temperature range
    :param speedSteps: Fan speed range
    :return: Fan speed value proportional to CPU temperature
    """
    # Calculate desired fan speed

    # Below first value, fan will run at min speed.
    if temp < tempSteps[0]:
        new_speed = speedSteps[0]
    # Above last value, fan will run at max speed
    elif temp >= tempSteps[len(tempSteps) - 1]:
        new_speed = speedSteps[len(tempSteps) - 1]
    # If temperature is between 2 steps, fan speed is calculated by linear interpolation
    else:
        for i in range(0, len(tempSteps) - 1):
            if (temp >= tempSteps[i]) and (temp < tempSteps[i + 1]):
                new_speed = round(
                    (speedSteps[i + 1] - speedSteps[i])
                    / (tempSteps[i + 1] - tempSteps[i])
                    * (temp - tempSteps[i])
                    + speedSteps[i],
                    1,
                )
    return new_speed
